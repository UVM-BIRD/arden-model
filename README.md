# Arden Model #

Arden Model is a library of Java objects used to represent language constructs in the Arden-syntax language.  This library is designed to be used in conjunction with the [Arden Parser](https://bitbucket.org/UVM-BIRD/arden-parser).

### Usage ###

Including the Arden Model library in your project can be accomplished by including the following dependency in your project's Maven POM:

    <dependency>
        <groupId>edu.uvm.ccts</groupId>
        <artifactId>arden-model</artifactId>
        <version>1.0.1</version>
    </dependency>

### Building Arden Model ###

Building the Arden Model library and making it available to other local Maven projects is accomplished by running the following:

    $ mvn package
    $ mvn install

### License and Copyright ###

The Arden Model library is Copyright 2015 [The University of Vermont and State Agricultural College](https://www.uvm.edu/), [Vermont Oxford Network](https://public.vtoxford.org/), and [The University of Vermont Medical Center](https://www.uvmhealth.org/medcenter/Pages/default.aspx).  All rights reserved.

The Arden Model library is licensed under the terms of the [GNU General Public License (GPL) version 3](https://www.gnu.org/licenses/gpl-3.0-standalone.html).