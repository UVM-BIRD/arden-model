/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of Arden Model.
 *
 * Arden Model is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Arden Model is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Arden Model.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.arden.model;

/**
 * Void.  Nothing.  The dark abyss.
 */
public class AVoid extends NonPrimaryTimeDataType<AVoid> {
    private static AVoid v = null;

    public static AVoid VOID() {
        if (v == null) v = new AVoid();
        return v;
    }

    private AVoid() {}

    public String toString() {
        return "void";
    }

    @Override
    public AVoid copy() {
        return this;
    }

    @Override
    public Object toJavaObject() {
        return null;
    }

    /**
     * Compares this object to another as an identity comparison, in which the primary time of the objects are
     * considered.
     * @param o
     * @return {@code true} if {@code this} and {@code o} have the same value <strong>and</strong> the same primary
     * time.
     */
    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (o == this) return true;
        return o.getClass() == getClass();
    }

    @Override
    public int hashCode() {
        return 995903;
    }

    /**
     * Compares this object to another, considering only the value of the object.  For {@link AVoid}, this
     * method is synonymous with {@link #equals}.
     * @param o
     * @return {@code true} if {@code this} and {@code o} are equal.
     */
    @Override
    public boolean hasValue(ADataType o) {
        return equals(o);
    }
}
