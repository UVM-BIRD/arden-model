/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of Arden Model.
 *
 * Arden Model is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Arden Model is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Arden Model.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.arden.model;

import edu.uvm.ccts.arden.util.ObjectUtil;

/**
 * An Arden data type
 * <br>
 * This is the base object for all Arden data types.
 */
public abstract class ADataType<T extends ADataType> {
    public abstract boolean hasValue(ADataType o);
    public abstract T copy();
    public abstract boolean hasPrimaryTime();
    public abstract Time getPrimaryTime();
    public abstract Object toJavaObject();

    public boolean isComparable() {
        return this instanceof Comparable;
    }

    public boolean isLike(ADataType obj) {
        return ObjectUtil.isLike(this, obj);
    }
}
