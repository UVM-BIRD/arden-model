/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of Arden Model.
 *
 * Arden Model is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Arden Model is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Arden Model.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.arden.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * An Arden-null object
 * <br>
 * Null is a special data type that signifies uncertainty. Such uncertainty may be the result of a lack of information
 * in the patient database or an explicit null value in the database. Null results from an error in execution, such as
 * a type mismatch or division by zero. Null may be specified explicitly within a slot using the word null (that is,
 * the null constant). Entities of data type null may also have a primary time.
 * @see 8.1
 */
public final class ANull extends PrimaryTimeDataType<ANull> {
    public static ANull NULL() {
        return new ANull();
    }

//////////////////////////////////////////////////////////////////////////////////

    public ANull() {
    }

    public ANull(ANull an) {
        primaryTime = an.primaryTime;
    }

    @Override
    public String toString() {
        return "null";
    }

    /**
     * Compares this object to another as an identity comparison, in which the primary time of the objects are
     * considered.
     * @param o
     * @return {@code true} if {@code this} and {@code o} have the same value <strong>and</strong> the same primary
     * time.
     */
    @Override
    public boolean equals(Object o) {
        if (o == null)                  return false;
        if (o == this)                  return true;
        if (o.getClass() != getClass()) return false;

        ANull an = (ANull) o;
        return new EqualsBuilder()
                .append(primaryTime, an.primaryTime)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(139, 127)
                .append(primaryTime)
                .toHashCode();
    }

    /**
     * Compares this object to another, considering only the value of the object, and not the primary time.
     * @param o
     * @return {@code true} if {@code this} and {@code o} have the same value.
     */
    @Override
    public boolean hasValue(ADataType o) {
        if (o == null) return false;
        if (o == this) return true;
        return o.getClass() == getClass();
    }

    @Override
    public ANull copy() {
        return new ANull(this);
    }

    @Override
    public Object toJavaObject() {
        return null;
    }
}
