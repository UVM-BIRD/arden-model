/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of Arden Model.
 *
 * Arden Model is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Arden Model is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Arden Model.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.arden.model;

import java.util.Calendar;

/**
 * An Arden-day-of-week object
 * <br>
 * The day-of-week data type is a special data type to represent specific days of the week to be used along with the
 * "day of week" operator. Values of this data type are either expressed by constants or by integer values.
 * <br>
 * Day-of-week constants are defined by the following keywords:
 * <ul>
 *     <li>MONDAY (1)</li>
 *     <li>TUESDAY (2)</li>
 *     <li>WEDNESDAY (3)</li>
 *     <li>THURSDAY (4)</li>
 *     <li>FRIDAY (5)</li>
 *     <li>SATURDAY (6)</li>
 *     <li>SUNDAY (7)</li>
 * </ul>
 * @see 8.12
 */
public enum DayOfWeek {
    MONDAY(1),
    TUESDAY(2),
    WEDNESDAY(3),
    THURSDAY(4),
    FRIDAY(5),
    SATURDAY(6),
    SUNDAY(7);

    private int val;

    public static DayOfWeek valueOf(int val) {
        switch (val) {
            case 1: return MONDAY;
            case 2: return TUESDAY;
            case 3: return WEDNESDAY;
            case 4: return THURSDAY;
            case 5: return FRIDAY;
            case 6: return SATURDAY;
            case 7: return SUNDAY;
            default: throw new RuntimeException("invalid DayOfWeek val: " + val);
        }
    }

    DayOfWeek(int val) {
        this.val = val;
    }

    public int toJavaCalendarValue() {
        switch (this) {
            case MONDAY:    return Calendar.MONDAY;
            case TUESDAY:   return Calendar.TUESDAY;
            case WEDNESDAY: return Calendar.WEDNESDAY;
            case THURSDAY:  return Calendar.THURSDAY;
            case FRIDAY:    return Calendar.FRIDAY;
            case SATURDAY:  return Calendar.SATURDAY;
            case SUNDAY:    return Calendar.SUNDAY;
            default:        throw new RuntimeException("invalid DayOfWeek val: " + val);
        }
    }

    public int intValue() {
        return val;
    }
}
