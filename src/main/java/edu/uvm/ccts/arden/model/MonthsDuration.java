/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of Arden Model.
 *
 * Arden Model is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Arden Model is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Arden Model.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.arden.model;

import edu.uvm.ccts.arden.util.DurationUtil;

/**
 * A {@link Duration} object, the granularity of which is expressed in months.
 */
public class MonthsDuration extends Duration<MonthsDuration> {
    private final ANumber months;

    public MonthsDuration(ANumber months) {
        this.months = months;
    }

    public MonthsDuration(MonthsDuration md) {
        primaryTime = md.primaryTime;
        months = md.months.copy();
    }

    public ANumber getMonths() {
        return months;
    }

    @Override
    public long getSeconds() {
        return months.multiply(SEC_IN_MONTH).longValue();
    }

    @Override
    public String toString() {
        return months.value() + " months";
    }

    public Duration add(Duration d) {
        // spec 8.5.2.4, 9.9.1

        if (d instanceof MonthsDuration) {
            ANumber n = months.add(((MonthsDuration) d).getMonths());
            return new MonthsDuration(n);

        } else {
            long x = DurationUtil.convertToSeconds(months, DurationUnit.MONTHS);
            long y = d.getSeconds();
            return new SecondsDuration(x + y);
        }
    }

    public Duration subtract(Duration d) {
        // spec 8.5.2.4, 9.9.3

        if (d instanceof MonthsDuration) {
            ANumber n = months.subtract(((MonthsDuration) d).getMonths());
            return new MonthsDuration(n);

        } else {
            long x = DurationUtil.convertToSeconds(months, DurationUnit.MONTHS);
            long y = d.getSeconds();
            return new SecondsDuration(x - y);
        }
    }

    @Override
    public Duration multiply(ANumber n) {
        return new MonthsDuration(months.multiply(n));
    }

    @Override
    public Duration multiply(Number n) {
        return multiply(new ANumber(n));
    }

    @Override
    public MonthsDuration copy() {
        return new MonthsDuration(this);
    }
}
