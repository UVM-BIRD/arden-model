/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of Arden Model.
 *
 * Arden Model is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Arden Model is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Arden Model.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.arden.model;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.regex.Pattern;

/**
 * An Arden-string object
 * <br>
 * Strings are streams of characters of variable length. String constants are defined in Section 7.1.6.
 * @see 8.6
 */
public class AString extends PrimaryTimeDataType<AString> implements Comparable<AString> {
    private final String value;

    public AString(String value) {
        this.value = value;
    }

    public AString(AString as) {
        primaryTime = as.primaryTime;
        value = as.value;
    }

    @Override
    public String toString() {
        return value;
    }

    /**
     * Compares this object to another as an identity comparison, in which the primary time of the objects are
     * considered.
     * @param o
     * @return {@code true} if {@code this} and {@code o} have the same value <strong>and</strong> the same primary
     * time.
     */
    @Override
    public boolean equals(Object o) {
        if (o == null)                  return false;
        if (o == this)                  return true;
        if (o.getClass() != getClass()) return false;

        AString as = (AString) o;
        return new EqualsBuilder()
                .append(primaryTime, as.primaryTime)
                .append(value, as.value)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(89, 41)
                .append(primaryTime)
                .append(value)
                .toHashCode();
    }

    /**
     * Compares this object to another, considering only the value of the object, and not the primary time.
     * @param o
     * @return {@code true} if {@code this} and {@code o} have the same value.
     */
    @Override
    public boolean hasValue(ADataType o) {
        if (o == null)                  return false;
        if (o == this)                  return true;
        if (o.getClass() != getClass()) return false;
        if (value == null)              return ((AString) o).value == null;

        return value.equalsIgnoreCase(((AString) o).value);
    }

    @Override
    public int compareTo(AString as) {
        if (this == as) return 0;
        return toString().compareTo(as.toString());
    }

    @Override
    public AString copy() {
        return new AString(this);
    }

    @Override
    public Object toJavaObject() {
        return value;
    }

    public AString concat(AString s) {          // do NOT preserve primary times!
        return new AString(value + s.value);
    }

    public ANumber length() {                   // do NOT preserve primary times!
        return new ANumber(value.length());
    }

    public AString uppercase() {
        AString s = new AString(value.toUpperCase());
        s.primaryTime = primaryTime;
        return s;
    }

    public AString lowercase() {
        AString s = new AString(value.toLowerCase());
        s.primaryTime = primaryTime;
        return s;
    }

    public AString trim() {
        AString s = new AString(StringUtils.strip(value, null));
        s.primaryTime = primaryTime;
        return s;
    }

    public AString trimLeft() {
        AString s = new AString(StringUtils.stripStart(value, null));
        s.primaryTime = primaryTime;
        return s;
    }

    public AString trimRight() {
        AString s = new AString(StringUtils.stripEnd(value, null));
        s.primaryTime = primaryTime;
        return s;
    }

    public ANumber findPositionOf(AString needle, int start) {
        return new ANumber(value.indexOf(needle.value, start - 1) + 1);     // must be case-sensitive!  also, do not
                                                                            // preserve primary time
    }

    public AString substring(int start, int numChars) {
        if (numChars < 0) {
            start += numChars + 1;
            numChars = numChars * -1;
        }

        int startIndex = start - 1;
        int endIndex = start + numChars - 1;
        if (endIndex > value.length()) endIndex = value.length();

        AString s = new AString(value.substring(startIndex, endIndex));
        s.primaryTime = primaryTime;                                        // maintain primary time

        return s;
    }

    public AList extractChars() {                               // do NOT maintain primary time!
        AList list = new AList();
        for (Character c : value.toCharArray()) {
            list.add(new AString(c.toString()));
        }
        return list;
    }

    /**
     * Perform case-insensitive matching against a string matching pattern.
     * <br>
     * {@code pattern} has two wildcard options.  First, it treats the underscore character ({@code _}) to mean
     * "any single arbitrary character."  Second, it treats the percent sign ({@code %}) to match zero or more arbitrary
     * characters.  These wildcard characters may be escaped (using the backslash character ({@code \}) to instruct
     * this function to treat the character as a regular character, and not as a wildcard.
     * @see 9.8.4
     * @param pattern
     * @return
     */
    public ABoolean matches(AString pattern) {
        Pattern p = buildPattern(pattern.toString());
        return new ABoolean(p.matcher(value).matches());
    }

    private Pattern buildPattern(String regex) {
        regex = regex.replaceAll("([.?*+\\\\^$\\[\\](){}])", "\\\\$1")  // escape regex special characters
                .replaceAll("^_", ".")                                  // replace leading _ with .
                .replaceAll("^%", ".*")                                 // replace leading % with .*
                .replaceAll("([^\\\\])_", "$1.")                        // replace unescaped _ with .
                .replaceAll("([^\\\\])%", "$1.*");                      // replace unescaped % with .*

        return Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
    }
}
