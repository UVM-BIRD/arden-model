/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of Arden Model.
 *
 * Arden Model is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Arden Model is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Arden Model.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.arden.model;

/**
 * An intermediate/abstract Arden-object data type which has a primary time.
 * <br>
 * While all objects that extend {@link ADataType} must implement {@link ADataType#getPrimaryTime}, only objects
 * that extend {@code PrimaryTimeDataType} are considered "atomic" elements and actually have primary times.
 * (This is due to the generic nature of processing required by the visitors - keeping primary time functions declared
 * in the top superclass simplifies parsing a lot).
 */
public abstract class PrimaryTimeDataType<T extends PrimaryTimeDataType> extends ADataType<T> {
    protected Long primaryTime = null;      // store in seconds - nice and lightweight, and ref-by-value!

    @Override
    public boolean hasPrimaryTime() {
        return primaryTime != null;
    }

    public Time getPrimaryTime() {
        if (primaryTime == null) {
            return null;

        } else {
            return new Time(primaryTime);
        }
    }

    public void setPrimaryTime(Time primaryTime) {
        if (primaryTime == null) {
            this.primaryTime = null;

        } else {
            this.primaryTime = primaryTime.getTimeInSeconds();
        }
    }
}
