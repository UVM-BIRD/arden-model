/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of Arden Model.
 *
 * Arden Model is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Arden Model is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Arden Model.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.arden.model;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * An Arden-object-type
 * <br>
 * Used for defining the structure of {@link AObject}.  Similar to Java {@link Class}, but for Arden stuff instead.
 */
public class AObjectType extends ADataType<AObjectType> {
    private String name;
    private List<String> attributes;

    public AObjectType(List<String> attributes) {
        this.attributes = Collections.unmodifiableList(attributes);
    }

    public AObjectType(AObjectType obj) {
        this.name = obj.name;
        this.attributes = Collections.unmodifiableList(new ArrayList<String>(obj.attributes));
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getAttributes() {
        return attributes;
    }

    @Override
    public AObjectType copy() {
        return new AObjectType(this);
    }

    @Override
    public boolean hasPrimaryTime() {
        return false;
    }

    @Override
    public Time getPrimaryTime() {
        return null;                    // does not implement primary time
    }

    @Override
    public String toString() {
        return "AObjectType<name=" + name + ", " +
                "attributes=[" + StringUtils.join(attributes, ", ") + "]>";
    }

    /**
     * Compares this object to another as an identity comparison.
     * @param o
     * @return {@code true} if {@code this} and {@code o} have the same value.
     */
    @Override
    public boolean equals(Object o) {
        if (o == null)                  return false;
        if (o == this)                  return true;
        if (o.getClass() != getClass()) return false;

        AObjectType aot = (AObjectType) o;
        return new EqualsBuilder()
                .append(name, aot.name)
                .append(attributes, aot.attributes)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(59, 71)
                .append(name)
                .append(attributes)
                .toHashCode();
    }

    /**
     * Compares this object to another, considering only the value of the object.  For {@link AObjectType}, this
     * method is synonymous with {@link #equals}.
     * @param o
     * @return {@code true} if {@code this} and {@code o} are equal.
     */
    @Override
    public boolean hasValue(ADataType o) {
        return equals(o);   // no primary time on this object, so has same functionality as equals
    }

    @Override
    public Object toJavaObject() {
        return null;
    }
}
