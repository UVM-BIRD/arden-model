/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of Arden Model.
 *
 * Arden Model is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Arden Model is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Arden Model.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.arden.model;

import edu.uvm.ccts.common.exceptions.UnhandledClassException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * An Arden List-of-Lists object
 * <br>
 * Normally, Arden doesn't allow lists to contain other lists.  However, this is a special case - see Arden spec
 * 11.2.1.6 for details.
 */
public class AListOfLists extends PrimaryTimeDataType<AListOfLists> implements Iterable<ADataType>, RowIterable {
    private List<ADataType> list = new ArrayList<ADataType>();

    public AListOfLists() {
    }

    public AListOfLists(int size) {
        for (int i = 0; i < size; i ++) {
            list.add(new AList());
        }
    }

    public AListOfLists(AListOfLists listOfLists) {
        for (ADataType item : listOfLists.list) {
            this.list.add(item.copy());
        }
    }

    public int size() {
        return list.size();
    }

    public void add(ADataType obj) {
        if (obj instanceof AList || obj instanceof ANull) {
            list.add(obj);

        } else {
            throw new UnhandledClassException("AListOfLists may only contain ANull and AList data types");
        }
    }

    @Override
    public int getColCount() {
        return list.size();
    }

    @Override
    public int getRowCount() {
        for (ADataType adt : list) {
            if (adt instanceof AList) {
                return ((AList) adt).size();
            }
        }
        return 0;
    }

    @Override
    public List<ADataType> getRow(int index) {
        List<ADataType> row = new ArrayList<ADataType>();

        for (ADataType adt : list) {
            if (adt instanceof AList) {
                row.add(((AList) adt).get(index + 1));

            } else {            // can only be ANull
                row.add(adt);
            }
        }

        return row;
    }

    public boolean isList(int index) {
        return list.get(index) instanceof AList;
    }

    public AList getList(int index) {
        return (AList) list.get(index);
    }

    public boolean isNull(int index) {
        return list.get(index) instanceof ANull;
    }

    public ANull getNull(int index) {
        return (ANull) list.get(index);
    }

    @Override
    public String toString() {
        List<String> list = new ArrayList<String>();
        for (ADataType obj : this.list) {
            list.add(obj.toString());
        }
        return "(" + StringUtils.join(list, ", ") + ")";
    }

    @Override
    public AListOfLists copy() {
        return new AListOfLists(this);
    }

    @Override
    public boolean equals(Object o) {
        if (o == null)                  return false;
        if (o == this)                  return true;
        if (o.getClass() != getClass()) return false;

        AListOfLists alol = (AListOfLists) o;
        return new EqualsBuilder()
                .append(list, alol.list)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(523, 193)
                .append(list)
                .toHashCode();
    }

    @Override
    public boolean hasValue(ADataType o) {
        if (o == null)                  return false;
        if (o == this)                  return true;
        if (o.getClass() != getClass()) return false;

        AListOfLists alol = (AListOfLists) o;
        if (list.size() != alol.list.size()) return false;

        for (int i = 0; i <= list.size(); i ++) {
            if ( ! list.get(i).hasValue(alol.list.get(i)) ) {
                return false;
            }
        }

        return true;
    }

    @Override
    public boolean hasPrimaryTime() {
        return getPrimaryTime() != null;
    }

    @Override
    public Time getPrimaryTime() {
        Time primaryTime = null;

        for (ADataType subList : list) {
            Time t = subList.getPrimaryTime();

            if (t == null) return null;

            if      (primaryTime == null)           primaryTime = t;
            else if ( ! t.hasValue(primaryTime) )   return null;
        }

        return primaryTime;
    }

    @Override
    public Object toJavaObject() {
        List<Object> list = new ArrayList<Object>();
        for (ADataType adt : this.list) {
            list.add(adt.toJavaObject());
        }
        return list;
    }

    @Override
    public Iterator<ADataType> iterator() {
        return list.iterator();
    }
}
