/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of Arden Model.
 *
 * Arden Model is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Arden Model is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Arden Model.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.arden.model;

public enum DataType {
    PRESENT,
    NULL,
    BOOLEAN,
    NUMBER,
    STRING,
    TIME,
    TIME_OF_DAY,
    DURATION,
    LIST,
    OBJECT
}
