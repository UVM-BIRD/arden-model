/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of Arden Model.
 *
 * Arden Model is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Arden Model is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Arden Model.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.arden.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * An Arden-time object
 * <br>
 * The time data type refers to points in absolute time; it is also referred to as timestamp in other systems. Both
 * date and time-of-day must be specified. Times back to the year 1800 must be supported and times before 1800-01-01
 * are not valid. Time constants (for example, 1990-07-12T00:00:00) are defined in Section 7.1.5.
 * @see 8.4
 */
public class Time extends PrimaryTimeDataType<Time> implements Comparable<Time>, ISimpleTimeComponent, ITimeComponent {
    private Calendar cal = Calendar.getInstance();
    private static final SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
    private static final SimpleDateFormat SQL_SDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    // hh:mm((:ss(.ddd)?)?(Z|(+|-)hh:mm)? - spec 6.1.8, 7.1.5, 7.1.11
    public Time(Date date) {
        cal.setTime(date);
    }

    public Time(Time t) {
        primaryTime = t.primaryTime;
        cal.setTimeInMillis(t.getTimeInMillis());
    }

    public Time(long timeInSeconds) {
        cal.setTimeInMillis(timeInSeconds * 1000);
    }

    @Override
    public String toString() {
        return SDF.format(cal.getTime());
    }

    public String toSql() {
        return "'" + SQL_SDF.format(cal.getTime()) + "'";
    }

    public long getTimeInSeconds() {
        return cal.getTime().getTime() / 1000;
    }

    public long getTimeInMillis() {
        return cal.getTime().getTime();
    }

    public ANumber getYear() {
        return new ANumber(cal.get(Calendar.YEAR));
    }

    public void setYear(ANumber year) {
        setYear(year.intValue());
    }

    public void setYear(int year) {
        cal.set(Calendar.YEAR, year);
    }

    public ANumber getMonth() {
        return new ANumber(cal.get(Calendar.MONTH) + 1);     // month is 0-based
    }

    public void setMonth(ANumber month) {
        setMonth(month.intValue());
    }

    public void setMonth(int month) {
        cal.set(Calendar.MONTH, month - 1);                 // month is 0-based
    }

    public ANumber getDay() {
        return new ANumber(cal.get(Calendar.DAY_OF_MONTH));
    }

    public void setDay(ANumber day) {
        setDay(day.intValue());
    }

    public void setDay(int day) {
        cal.set(Calendar.DAY_OF_MONTH, day);
    }

    public TimeOfDay getTimeOfDay() {
        return new TimeOfDay(this);
    }

    public void setTimeOfDay(TimeOfDay tod) {
        setHour(tod.getHour());
        setMinute(tod.getMinute());
        setSecond(tod.getSecond());
    }

    public ANumber getHour() {
        return new ANumber(cal.get(Calendar.HOUR_OF_DAY));
    }

    public void setHour(ANumber hour) {
        setHour(hour.intValue());
    }

    public void setHour(int hour) {
        cal.set(Calendar.HOUR_OF_DAY, hour);
    }

    public ANumber getMinute() {
        return new ANumber(cal.get(Calendar.MINUTE));
    }

    public void setMinute(ANumber minute) {
        setMinute(minute.intValue());
    }

    public void setMinute(int minute) {
        cal.set(Calendar.MINUTE, minute);
    }

    public ANumber getSecond() {
        return new ANumber(cal.get(Calendar.SECOND));
    }

    public void setSecond(ANumber second) {
        cal.set(Calendar.SECOND, second.intValue());
        if (second.isFractionalNumber()) {
            int ms = (int) ((second.asFractionalNumber() - second.intValue()) * 1000);
            cal.set(Calendar.MILLISECOND, ms);
        }
    }

    public void setSecond(int second) {
        cal.set(Calendar.SECOND, second);
    }

    public int getDaysInMonth() {
        return cal.getActualMaximum(Calendar.DAY_OF_MONTH);
    }

    @Override
    public Time asTime(Time relativeToTime) {

        // todo : shouldn't this do something with relativeToTime?

        return copy();
    }

    /**
     * Compares this object to another as an identity comparison, in which the primary time of the objects are
     * considered.
     * @param o
     * @return {@code true} if {@code this} and {@code o} have the same value <strong>and</strong> the same primary
     * time.
     */
    @Override
    public boolean equals(Object o) {
        if (o == null)                  return false;
        if (o == this)                  return true;
        if (o.getClass() != getClass()) return false;

        Time t = (Time) o;
        return new EqualsBuilder()
                .append(primaryTime, t.primaryTime)
                .append(getTimeInMillis(), t.getTimeInMillis())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(23, 89)
                .append(primaryTime)
                .append(getTimeInMillis())
                .toHashCode();
    }

    /**
     * Compares this object to another, considering only the value of the object, and not the primary time.
     * @param o
     * @return {@code true} if {@code this} and {@code o} have the same value.
     */
    @Override
    public boolean hasValue(ADataType o) {
        if (o == null)                  return false;
        if (o == this)                  return true;
        if (o.getClass() != getClass()) return false;

        return getTimeInMillis() == ((Time) o).getTimeInMillis();
    }

    @Override
    public int compareTo(Time t) {
        if (this == t) return 0;

        long a = getTimeInMillis();
        long b = t.getTimeInMillis();

        if (a < b) return -1;
        else if (a > b) return 1;

        assert this.equals(t) : "compareTo inconsistent with equals.";

        return 0;
    }

    @Override
    public Time copy() {
        return new Time(this);
    }

    public Date asDate() {
        return cal.getTime();
    }

    @Override
    public Object toJavaObject() {
        return asDate();
    }

    public SecondsDuration subtract(Time t) {
        // spec 8.5.2.1, 9.9.3
        return new SecondsDuration(getTimeInSeconds() - t.getTimeInSeconds());
    }

    public DayOfWeek getDayOfWeek() {
        switch (cal.get(Calendar.DAY_OF_WEEK)) {
            case Calendar.MONDAY:       return DayOfWeek.MONDAY;
            case Calendar.TUESDAY:      return DayOfWeek.TUESDAY;
            case Calendar.WEDNESDAY:    return DayOfWeek.WEDNESDAY;
            case Calendar.THURSDAY:     return DayOfWeek.THURSDAY;
            case Calendar.FRIDAY:       return DayOfWeek.FRIDAY;
            case Calendar.SATURDAY:     return DayOfWeek.SATURDAY;
            default:                    return DayOfWeek.SUNDAY;
        }
    }

    public Time add(Duration d) {
        return addOrSubtract(d, true);
    }

    public Time subtract(Duration d) {
        return addOrSubtract(d, false);
    }

    private Time addOrSubtract(Duration d, boolean adding) {
        // spec 8.5.2.2, 8.5.2.3, 9.9.1

        int mult = adding ? 1 : -1;

        if (d instanceof MonthsDuration) {
            Calendar c = Calendar.getInstance();
            c.setTime(cal.getTime());

            ANumber n = ((MonthsDuration) d).getMonths();
            if (n.isWholeNumber()) {
                c.add(Calendar.MONTH, mult * n.intValue());

            } else {
                int months = n.intValue();
                int seconds = n.subtract(months).multiply(Duration.SEC_IN_MONTH).intValue();

                c.add(Calendar.MONTH, mult * months);
                c.add(Calendar.SECOND, mult * seconds);
            }

            return new Time(c.getTime());

        } else {
            long t = d.getSeconds() * 1000;

            Date date = new Date();
            date.setTime(getTimeInMillis() + mult * t);

            return new Time(date);
        }
    }
}
