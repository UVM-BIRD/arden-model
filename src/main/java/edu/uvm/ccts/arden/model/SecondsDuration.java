/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of Arden Model.
 *
 * Arden Model is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Arden Model is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Arden Model.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.arden.model;

import edu.uvm.ccts.arden.util.DurationUtil;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * A {@link Duration} object, the granularity of which is expressed in seconds.
 */
public class SecondsDuration extends Duration<SecondsDuration> {
    private final long seconds;

    public SecondsDuration(long seconds) {
        this.seconds = seconds;
    }

    public SecondsDuration(SecondsDuration sd) {
        primaryTime = sd.primaryTime;
        seconds = sd.seconds;
    }

    public long getSeconds() {
        return seconds;
    }

    @Override
    public String toString() {
        long secs = getSeconds();
        List<String> list = new ArrayList<String>();

        if (secs >= SEC_IN_YEAR || secs <= SEC_IN_YEAR * -1) {
            long l = secs / SEC_IN_YEAR;
            list.add(l + (l == 1 || l == -1 ? " year" : " years"));
            secs = secs % SEC_IN_YEAR;
        }

        if (secs >= SEC_IN_MONTH || secs <= SEC_IN_MONTH * -1) {
            long l = secs / SEC_IN_MONTH;
            list.add(l + (l == 1 || l == -1 ? " month" : " months"));
            secs = secs % SEC_IN_MONTH;
        }

        if (secs >= SEC_IN_DAY || secs <= SEC_IN_DAY * -1) {
            long l = secs / SEC_IN_DAY;
            list.add(l + (l == 1 || l == -1 ? " day" : " days"));
            secs = secs % SEC_IN_DAY;
        }

        if (secs >= SEC_IN_HOUR || secs <= SEC_IN_HOUR * -1) {
            long l = secs / SEC_IN_HOUR;
            list.add(l + (l == 1 || l == -1 ? " hour" : " hours"));
            secs = secs % SEC_IN_HOUR;
        }

        if (secs >= SEC_IN_MINUTE || secs <= SEC_IN_MINUTE * -1) {
            long l = secs / SEC_IN_MINUTE;
            list.add(l + (l == 1 || l == -1 ? " minute" : " minutes"));
            secs = secs % SEC_IN_MINUTE;
        }

        if (secs > 0 || list.isEmpty()) {
            list.add(secs + (secs == 1 || secs == -1 ? " second" : " seconds"));
        }

        return StringUtils.join(list, " ");
    }

    public Duration add(Duration d) {
        // spec 8.5.2.4, 9.9.1

        if (d instanceof MonthsDuration) {
            long s = DurationUtil.convertToSeconds(((MonthsDuration) d).getMonths(), DurationUnit.MONTHS);
            return new SecondsDuration(seconds + s);

        } else {
            return new SecondsDuration(seconds + d.getSeconds());
        }
    }

    public Duration subtract(Duration d) {
        // spec 8.5.2.4, 9.9.3

        if (d instanceof MonthsDuration) {
            long s = DurationUtil.convertToSeconds(((MonthsDuration) d).getMonths(), DurationUnit.MONTHS);
            return new SecondsDuration(seconds - s);

        } else {
            return new SecondsDuration(seconds - d.getSeconds());
        }
    }

    @Override
    public Duration multiply(ANumber n) {
        return new SecondsDuration(n.multiply(seconds).asWholeNumber());
    }

    @Override
    public Duration multiply(Number n) {
        return multiply(new ANumber(n));
    }

    @Override
    public SecondsDuration copy() {
        return new SecondsDuration(this);
    }
}
