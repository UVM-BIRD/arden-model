/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of Arden Model.
 *
 * Arden Model is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Arden Model is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Arden Model.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.arden.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.Calendar;
import java.util.Date;

/**
 * An Arden-time-of-day object
 * <br>
 * The time-of-day data type refers to points in time that are not directly linked to a specific date. Time-of-day
 * constants are analogously defined to time constants leaving the date portion blank. Time-of-day constants (for
 * example, 23:20:00) are defined in Section 7.1.6.
 * <br>
 * Operators that can use both time arguments and time-of-day arguments at the same time may follow the default
 * time-of-day handling as defined in Section 9.1.5 .The primary time handling is unaffected by these extension.
 * @see 8.11
 */
public class TimeOfDay extends PrimaryTimeDataType<TimeOfDay> implements Comparable<TimeOfDay> {
    private ANumber hour;
    private ANumber minute;
    private ANumber second;

    public TimeOfDay(Time time) {
        // do not set primary time!  see spec 9.10.5
        hour = time.getHour();
        minute = time.getMinute();
        second = time.getSecond();
    }

    public TimeOfDay(TimeOfDay tod) {
        primaryTime = tod.primaryTime;
        hour = tod.getHour().copy();
        minute = tod.getMinute().copy();
        second = tod.getSecond().copy();
    }

    public TimeOfDay(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        hour = new ANumber(cal.get(Calendar.HOUR_OF_DAY));
        minute = new ANumber(cal.get(Calendar.MINUTE));
        second = new ANumber(cal.get(Calendar.SECOND));
    }

    @Override
    public String toString() {
        return String.format("%02d:%02d:%02d", hour.intValue(), minute.intValue(), second.intValue());
    }

    public ANumber getHour() {
        return hour;
    }

    public ANumber getMinute() {
        return minute;
    }

    public ANumber getSecond() {
        return second;
    }

    public int getSecondsSinceMidnight() {
        int h = hour.intValue();
        int m = minute.intValue();
        int s = second.intValue();

        return (h * Duration.SEC_IN_HOUR) + (m * Duration.SEC_IN_MINUTE) * s;
    }

    /**
     * Compares this object to another as an identity comparison, in which the primary time of the objects are
     * considered.
     * @param o
     * @return {@code true} if {@code this} and {@code o} have the same value <strong>and</strong> the same primary
     * time.
     */
    @Override
    public boolean equals(Object o) {
        if (o == null)                  return false;
        if (o == this)                  return true;
        if (o.getClass() != getClass()) return false;

        TimeOfDay tod = (TimeOfDay) o;
        return new EqualsBuilder()
                .append(primaryTime, tod.primaryTime)
                .append(hour, tod.hour)
                .append(minute, tod.minute)
                .append(second, tod.second)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(181, 137)
                .append(primaryTime)
                .append(hour)
                .append(minute)
                .append(second)
                .toHashCode();
    }

    /**
     * Compares this object to another, considering only the value of the object, and not the primary time.
     * @param o
     * @return {@code true} if {@code this} and {@code o} have the same value.
     */
    @Override
    public boolean hasValue(ADataType o) {
        if (o == null)                  return false;
        if (o == this)                  return true;
        if (o.getClass() != getClass()) return false;

        TimeOfDay tod = (TimeOfDay) o;
        return hour.hasValue(tod.hour) &&
                minute.hasValue(tod.minute) &&
                second.hasValue(tod.second);
    }

    @Override
    public int compareTo(TimeOfDay tod) {
        if (this == tod) return 0;

        int a = getSecondsSinceMidnight();
        int b = tod.getSecondsSinceMidnight();

        if (a < b) return -1;
        else if (a > b) return 1;

        assert this.equals(tod) : "compareTo inconsistent with equals.";

        return 0;
    }

    @Override
    public TimeOfDay copy() {
        return new TimeOfDay(this);
    }

    @Override
    public Object toJavaObject() {
        return getSecondsSinceMidnight();
    }
}
