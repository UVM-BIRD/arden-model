/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of Arden Model.
 *
 * Arden Model is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Arden Model is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Arden Model.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.arden.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by mstorer on 11/19/13.
 */
public class ArgumentList extends NonPrimaryTimeDataType<ArgumentList> implements Iterable<ADataType> {
    private List<ADataType> list;

    public ArgumentList() {
        list = new ArrayList<ADataType>();
    }

    public ArgumentList(List<ADataType> list) {
        this.list = list;
    }

    public ArgumentList(ArgumentList argumentList) {
        list = new ArrayList<ADataType>();
        for (ADataType item : argumentList.list) {
            list.add(item.copy());
        }
    }

    public List<ADataType> getList() {
        return list;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null)                  return false;
        if (o == this)                  return true;
        if (o.getClass() != getClass()) return false;

        ArgumentList al = (ArgumentList) o;
        return new EqualsBuilder()
                .append(list, al.list)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(23, 499)
                .append(list)
                .toHashCode();
    }

    @Override
    public boolean hasValue(ADataType o) {
        return false;
    }

    @Override
    public ArgumentList copy() {
        return new ArgumentList(this);
    }

    @Override
    public Object toJavaObject() {
        List<Object> list = new ArrayList<Object>();
        for (ADataType adt : this.list) {
            list.add(adt.toJavaObject());
        }
        return list;
    }

    @Override
    public Iterator<ADataType> iterator() {
        return list.iterator();
    }
}
