/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of Arden Model.
 *
 * Arden Model is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Arden Model is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Arden Model.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.arden.model;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by mstorer on 10/30/13.
 */
public class Variables {
    private Map<String, ADataType> variables = new LinkedHashMap<String, ADataType>();

    public ADataType getVariable(String name) {
        return variables.containsKey(name) ?
                variables.get(name) :
                new ANull();
    }

    public void putVariable(String name, ADataType obj) {
        variables.put(name, obj);
    }

    public Set<String> getVariableNames() {
        return variables.keySet();
    }

    public boolean hasVariable(String name) {
        return variables.containsKey(name);
    }
}
