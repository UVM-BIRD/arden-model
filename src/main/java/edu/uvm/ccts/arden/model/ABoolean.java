/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of Arden Model.
 *
 * Arden Model is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Arden Model is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Arden Model.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.arden.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * An Arden-boolean object
 * <br>
 * The Boolean data type includes the two truth values: true and false. The word true signifies Boolean true and the
 * word false signifies Boolean false.
 * <br>
 * The logical operators use tri-state logic by using null to signify the third state, uncertainty. For example, true
 * or null is true. Although null is uncertain, a disjunction that includes true is always true regardless of the other
 * arguments. However, false or null is null because false in a disjunction adds no information. See Section 9.4 for
 * full truth tables.
 * @see 8.2
 */
public class ABoolean extends PrimaryTimeDataType<ABoolean> {
    public static ABoolean TRUE() {
        return new ABoolean(true);
    }

    public static ABoolean FALSE() {
        return new ABoolean(false);
    }

    public static ABoolean UNKNOWN() {
        return new ABoolean();
    }

//////////////////////////////////////////////////////////////////////////////////

    private final Boolean value;

    public ABoolean() {
        value = null;
    }

    public ABoolean(Boolean value) {
        this.value = value;
    }

    public ABoolean(ABoolean ab) {
        primaryTime = ab.primaryTime;
        value = ab.value;
    }

    public Boolean asBoolean() {
        return value;
    }

    public Integer asInteger() {
        if (isUnknown()) {
            return null;

        } else {
            return isTrue() ? 1 : 0;
        }
    }

    @Override
    public String toString() {
        return value == null ? "null" : value.toString();
    }

    /**
     * Compares this object to another as an identity comparison, in which the primary time of the objects are
     * considered.
     * @param o
     * @return {@code true} if {@code this} and {@code o} have the same value <strong>and</strong> the same primary
     * time.
     */
    @Override
    public boolean equals(Object o) {
        if (o == null)                  return false;
        if (o == this)                  return true;
        if (o.getClass() != getClass()) return false;

        ABoolean ab = (ABoolean) o;
        return new EqualsBuilder()
                .append(primaryTime, ab.primaryTime)
                .append(value, ab.value)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(167, 137)
                .append(primaryTime)
                .append(value)
                .toHashCode();
    }

    /**
     * Compares this object to another, considering only the value of the object, and not the primary time.
     * @param o
     * @return {@code true} if {@code this} and {@code o} have the same value.
     */
    @Override
    public boolean hasValue(ADataType o) {
        if (o == null)                  return false;
        if (o == this)                  return true;
        if (o.getClass() != getClass()) return false;
        if (value == null)              return ((ABoolean) o).value == null;

        return value.equals(((ABoolean) o).value);
    }

    @Override
    public ABoolean copy() {
        return new ABoolean(this);
    }

    @Override
    public Object toJavaObject() {
        return value;
    }

    public ABoolean not() {
        if (isUnknown())    return ABoolean.UNKNOWN();
        else                return value ? ABoolean.FALSE() : ABoolean.TRUE();
    }

    public boolean isUnknown() {
        return value == null;
    }

    public boolean isTrue() {
        return value != null && value;
    }

    public boolean isFalse() {
        return value != null && ! value;
    }
}
