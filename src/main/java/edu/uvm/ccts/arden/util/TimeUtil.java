/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of Arden Model.
 *
 * Arden Model is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Arden Model is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Arden Model.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.arden.util;

import edu.uvm.ccts.arden.model.*;
import edu.uvm.ccts.common.util.DateUtil;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by mstorer on 8/13/13.
 */
public class TimeUtil {
    public static Time parseTime(String s) throws ParseException {
        return new Time(DateUtil.parseDate(s));
    }

    public static TimeOfDay parseTimeOfDay(String s) throws ParseException {
        return new TimeOfDay(parseTime(s));
    }

    public static Time mean(List<Time> list) {
        if (list == null || list.isEmpty()) return null;

        long total = 0L;
        for (Time t : list) {
            total += t.getTimeInSeconds();
        }

        Time mean = new Time(total / list.size());
        mean.setPrimaryTime(ListUtil.getPrimaryTime(list));         // maintain primary time

        return mean;
    }

    public static Time median(List<Time> list) {
        if (list == null || list.isEmpty()) return null;

        list = shallowCopy(list);
        Collections.sort(list);

        if (list.size() % 2 == 0) {     // even
            int index = list.size() / 2 - 1;
            Duration d1 = list.get(index + 1).subtract(list.get(index));
            Duration d2 = new SecondsDuration(d1.getSeconds() / 2);

            Time median = list.get(index).add(d2);
            median.setPrimaryTime(ListUtil.getPrimaryTime(list.subList(index, index + 1)));

            return median;

        } else {
            int index = ((list.size() - 1) / 2);
            return list.get(index);
        }
    }

////////////////////////////////////////////////////////////////////////////////////////
// Private methods
//

    private static List<Time> shallowCopy(List<Time> list) {
        if (list == null) return null;

        List<Time> copied = new ArrayList<Time>();
        copied.addAll(list);
        return copied;
    }

    /**
     * @deprecated use {@link AList#minimum} instead
     * @param list
     * @return
     */
    @Deprecated
    public static Time min(List<Time> list) {
        if (list == null || list.isEmpty()) return null;

        Time min = null;
        for (Time t : list) {
            if (min == null)                min = t;
            else if (t.compareTo(min) < 0)  min = t;
        }

        return min;
    }

    /**
     * @deprecated use {@link AList#maximum} instead
     * @param list
     * @return
     */
    @Deprecated
    public static Time max(List<Time> list) {
        if (list == null || list.isEmpty()) return null;

        Time max = null;
        for (Time t : list) {
            if (max == null)                max = t;
            else if (t.compareTo(max) > 0)  max = t;
        }

        return max;
    }
}
