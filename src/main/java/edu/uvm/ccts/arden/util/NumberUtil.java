/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of Arden Model.
 *
 * Arden Model is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Arden Model is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Arden Model.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.arden.util;

import edu.uvm.ccts.arden.model.ANumber;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by mstorer on 8/13/13.
 */
public class NumberUtil {
    public static ANumber parseNumber(String s) {
        boolean isFloat = s.indexOf('.') >= 0 || s.indexOf('e') >= 0 || s.indexOf('E') >= 0;
        if (isFloat) {
            return new ANumber(Double.parseDouble(s));

        } else {
            return new ANumber(Long.parseLong(s));
        }
    }

    public static boolean isWholeNumber(Number n) {
        if (n == null) return false;

        double dv = n.doubleValue();
        long lv = n.longValue();

        if (dv > 0) {
            return Math.abs(dv - lv) <= 0.0000001 || Math.abs(lv + 1 - dv) <= 0.0000001;

        } else {
            return Math.abs(dv - lv) <= 0.0000001 || Math.abs(lv - 1 - dv) <= 0.0000001;
        }
    }

    public static Long round(Number n) {
        if (n == null) return null;

        double dv = n.doubleValue();
        long lv = n.longValue();

        if (Math.abs(dv - lv) < 0.5)    return lv;
        else                            return dv < 0 ? lv - 1 : lv + 1;
    }

    public static ANumber mean(List<ANumber> list) {
        if (list == null || list.isEmpty()) return null;

        ANumber mean = sum(list).divide(list.size());
        mean.setPrimaryTime(ListUtil.getPrimaryTime(list));     // maintain primary time

        return mean;
    }

    public static ANumber median(List<ANumber> list) {
        if (list == null || list.isEmpty()) return null;

        List<ANumber> sortedList = shallowCopy(list);           // ensure we don't sort the source list!
        Collections.sort(sortedList);

        if (sortedList.size() % 2 == 0) {     // even
            int index = sortedList.size() / 2 - 1;
            ANumber total = sortedList.get(index).add(sortedList.get(index + 1));

            ANumber median = total.divide(2);
            median.setPrimaryTime(ListUtil.getPrimaryTime(sortedList.subList(index, index + 1)));

            return median;

        } else {
            int index = ((sortedList.size() - 1) / 2);
            return sortedList.get(index);
        }
    }

    public static ANumber sum(List<ANumber> list) {
        if (list == null) return null;

        ANumber sum = new ANumber(0);
        for (ANumber n : list) {
            sum = sum.add(n);
        }

        sum.setPrimaryTime(ListUtil.getPrimaryTime(list));

        return sum;
    }

    public static ANumber variance(List<ANumber> list) {
        if (list == null) return null;

        ANumber mean = mean(list);
        double f = 0;
        for (ANumber n : list) {
            ANumber diff = mean.subtract(n);
            f += diff.raiseToPower(2).doubleValue();
        }

        ANumber variance = new ANumber(f / list.size());
        variance.setPrimaryTime(ListUtil.getPrimaryTime(list));

        return variance;
    }

    public static ANumber stddev(List<ANumber> list) {
        ANumber stddev = new ANumber(Math.sqrt(variance(list).doubleValue()));
        stddev.setPrimaryTime(ListUtil.getPrimaryTime(list));

        return stddev;
    }

    /**
     * @deprecated use {@link edu.uvm.ccts.arden.model.AList#minimum} instead
     * @param list
     * @return
     */
    @Deprecated
    public static ANumber min(List<ANumber> list) {
        if (list == null || list.isEmpty()) return null;

        ANumber min = null;
        for (ANumber n : list) {
            if (min == null)                 min = n;
            else if (n.compareTo(min) < 0)   min = n;
        }

        return min;
    }

    /**
     * @deprecated use {@link edu.uvm.ccts.arden.model.AList#maximum} instead
     * @param list
     * @return
     */
    @Deprecated
    public static ANumber max(List<ANumber> list) {
        if (list == null || list.isEmpty()) return null;

        ANumber max = null;
        for (ANumber n : list) {
            if (max == null)                 max = n;
            else if (n.compareTo(max) > 0)   max = n;
        }

        return max;
    }

    private static List<ANumber> shallowCopy(List<ANumber> list) {
        if (list == null) return null;

        List<ANumber> copied = new ArrayList<ANumber>();
        copied.addAll(list);
        return copied;
    }
}
