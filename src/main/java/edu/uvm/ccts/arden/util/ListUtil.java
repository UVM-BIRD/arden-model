/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of Arden Model.
 *
 * Arden Model is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Arden Model is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Arden Model.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.arden.util;

import edu.uvm.ccts.arden.model.ADataType;
import edu.uvm.ccts.arden.model.Time;

import java.util.List;

/**
 * Created by mstorer on 8/22/13.
 */
public class ListUtil {

    /**
     * getPrimaryTime
     * @param list
     * @return a {@link edu.uvm.ccts.arden.model.Time} object if all list items have the same primary time.  returns {@code null} otherwise.
     */
    public static Time getPrimaryTime(List<? extends ADataType> list) {
        if (list == null) return null;

        Time primaryTime = null;

        for (ADataType item : list) {
            Time t = item.getPrimaryTime();

            if      (t == null)                     return null;
            else if (primaryTime == null)           primaryTime = t;
            else if ( ! t.hasValue(primaryTime) )   return null;
        }

        return primaryTime;
    }
}
