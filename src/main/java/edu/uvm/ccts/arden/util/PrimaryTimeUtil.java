/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of Arden Model.
 *
 * Arden Model is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Arden Model is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Arden Model.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.arden.util;

import edu.uvm.ccts.arden.model.PrimaryTimeDataType;
import edu.uvm.ccts.arden.model.Time;

/**
 * Created by mstorer on 6/5/14.
 */
public class PrimaryTimeUtil {

    /**
     * Returns the primary time for all objects passed in as parameters.  If any parameter objects do not have a
     * primary time, or if any primary time differs from any other, then {@code null} is returned.
     * @see 9.1.4
     * @param objs
     * @return
     */
    public static Time getPrimaryTime(PrimaryTimeDataType ... objs) {
        if      (objs == null)      return null;
        else if (objs.length == 0)  return null;
        else if (objs.length == 1)  return objs[0].getPrimaryTime();
        else {
            Time pt = null;
            for (PrimaryTimeDataType obj : objs) {
                if      (obj.getPrimaryTime() == null)          return null;
                else if (pt == null)                            pt = obj.getPrimaryTime();
                else if ( ! pt.equals(obj.getPrimaryTime()) )   return null;
            }
            return pt;
        }
    }
}
